# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-11-03 21:46
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0005_auto_20171103_0042'),
    ]

    operations = [
        migrations.AddField(
            model_name='buy',
            name='text_buy',
            field=models.TextField(default=1, verbose_name='Продано товаров и услуг'),
            preserve_default=False,
        ),
    ]
