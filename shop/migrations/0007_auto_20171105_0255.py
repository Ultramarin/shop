# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-11-05 00:55
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0006_buy_text_buy'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='buy',
            options={'verbose_name': 'Покупка', 'verbose_name_plural': 'Таблица покупок'},
        ),
        migrations.AlterModelOptions(
            name='client',
            options={'verbose_name': 'Клиент', 'verbose_name_plural': 'Таблица клиентов'},
        ),
        migrations.AlterModelOptions(
            name='item',
            options={'verbose_name': 'Товар', 'verbose_name_plural': 'Таблица товаров'},
        ),
        migrations.AlterModelOptions(
            name='services',
            options={'verbose_name': 'Услуга/ремонт', 'verbose_name_plural': 'Таблица услуг и ремонтов'},
        ),
        migrations.AlterModelOptions(
            name='store',
            options={'verbose_name': 'Магазин', 'verbose_name_plural': 'Магазины'},
        ),
        migrations.RemoveField(
            model_name='buy',
            name='text_buy',
        ),
        migrations.AddField(
            model_name='buy',
            name='item',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='shop.Item', verbose_name='Продано товаров и услуг'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='buy',
            name='service',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='shop.Services', verbose_name='Услуги'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='services',
            name='currency',
            field=models.CharField(choices=[('uan', 'ГРН'), ('usd', 'USD'), ('eur', 'EUR')], max_length=255, verbose_name='Валюта'),
        ),
    ]
