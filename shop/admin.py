from django.contrib import admin
from .models import Store, Item, Buy, Client, Services

class StoreAdmin(admin.ModelAdmin):
    list_display = ('name', 'address')
    search_fields = ('name',)
    list_filter = ('name',)

class ItemAdmin(admin.ModelAdmin):
    list_display = ('name', 'storage', 'price', 'amount', 'coment' )
    search_fields = ('name',)
    list_filter = ('name', 'storage')

class BuyAdmin(admin.ModelAdmin):
    list_display = ('sellerName', "shopName", "get_item", "get_service", "price", "pub_date")
    list_filter = ("sellerName", "shopName", "pub_date")
    filter_horizontal = ('item', "service")

class ClientAdmin(admin.ModelAdmin):
    list_display = ("name", "number_phone", "email", "discont", "coment")
    search_fields = ('name',)
    list_filter = ("name",)

class ServiceAdmin(admin.ModelAdmin):
    list_display = ("name", "price", "currency", "coment")
    search_fields = ('name',)
    list_filter = ("name",)

admin.site.register(Store, StoreAdmin)
admin.site.register(Item, ItemAdmin)
admin.site.register(Buy, BuyAdmin)
admin.site.register(Client, ClientAdmin)
admin.site.register(Services, ServiceAdmin)
# Register your models here.
