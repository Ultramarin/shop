from django.http import HttpResponse
from .models import Item, Buy, Store
from django.template import RequestContext, loader
from django.shortcuts import render
def detali(request):
    """
    
    :param request: 
    :return: 
    """
    ItemList = Item.objects.all()
    template = loader.get_template("shop/detali.html")
    context = {"latest_question_list":ItemList}
    return render(request, "shop/detali.html", context)

def find(request):
    """
    
    :param request: 
    :return: 
    """
    #if request.method == 'POST':
    #    return render(request,"shop/detali.html", {"latest_question_list":Item.objects.all()})
    try:
        find_item = request.POST['find']
    except:
        return render(request, "shop/detali.html")
    find_item = find_item.lower()
    ItemList = Item.objects.all()
    l = []
    for item in ItemList:
        st = item.name.lower()
        if st.find(find_item) != -1:
          l.append(item)
    context = {"latest_question_list": l}
    return render(request, "shop/detali.html", context)

def buy(request):
    """
    
    :return: 
    """
    try:
        shop = request.POST['shop']
        seler = request.POST['seller']
        text = request.POST['item']
        price = request.POST['price']
    except:
        return HttpResponse("buy")

    buy = Buy(shopName=Store.objects.get(name=shop), sellerName=seler, text_buy=text, price=float(price))
    buy.save()
    return HttpResponse("sucses")

def index(request):
    """"""
    return render(request, "shop/index.html")

def service(request):
    return render(request, "shop/detali.html")