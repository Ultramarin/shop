from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^detali', views.detali, name='detali'),
    url(r'^buy', views.buy, name='buy'),
    url(r'^$', views.index, name='index'),
    url(r'^find', views.find, name='find'),
    url(r'^service', views.service, name="service")
]