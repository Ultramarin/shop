from django.db import models
from datetime import timezone
#from django.core.exceptions import RegexValidator

class Store(models.Model):
    """
    Таблица 
    """
    name = models.CharField(max_length=255, verbose_name="Названия магазина")
    address = models.CharField(max_length=255, verbose_name="Адрес")

    class Meta:
        verbose_name = "Магазин"
        verbose_name_plural = "Магазины"

    def __str__(self):
        return self.name

class Item(models.Model):
    """
    Таблица товаров
    """
    storage = models.ForeignKey(Store, verbose_name="Магазин", on_delete=models.CASCADE)
    name = models.CharField(max_length=255, verbose_name="Названия товара")
    price = models.FloatField(default=0, verbose_name="Цена")
    amount = models.IntegerField(default=0, verbose_name="Количество")
    coment = models.CharField(max_length=255, blank=True, verbose_name="Коментарий")

    class Meta:
        verbose_name = "Товар"
        verbose_name_plural = "Таблица товаров"

    def __str__(self):
        return "%s, %s" %(self.name, str(self.storage))#"%s %s %s %s " % (self.name, str(self.storage),str(self.price), str(self.amount))

class Services(models.Model):
    """
    Таблица услуг
    """
    name = models.CharField(max_length=255, verbose_name="Названия услуги")
    price = models.FloatField(verbose_name="Цена услуги", default=0)
    currency = models.CharField(max_length=255, verbose_name="Валюта", choices=[("uan","ГРН"), ("usd", "USD"), ("eur", "EUR")])
    coment = models.CharField(max_length=255, verbose_name="Коментарий", blank=True)

    class Meta:
        verbose_name = "Услуга/ремонт"
        verbose_name_plural = "Таблица услуг и ремонтов"

    def __str__(self):
        return self.name

class Buy(models.Model):
    """
    Таблица покупок
    """
    sellerName = models.CharField(max_length=255, verbose_name='Имя продавца')
    item = models.ManyToManyField(Item, verbose_name="Продано товаров",  blank=True)
    service = models.ManyToManyField(Services, verbose_name="Услуги",  blank=True)
    shopName = models.ForeignKey(Store, verbose_name="Магазин")
    price = models.FloatField(default=0, verbose_name="Продано на суму")
    pub_date = models.DateTimeField( auto_now=True, verbose_name="Дата продажи")

    class Meta:
        verbose_name = "Покупка"
        verbose_name_plural = "Таблица покупок"

    def get_item(self):
        item_list = self.item.get_queryset()
        s = ''
        for x in item_list:
            s +=",   %s %s" %(x.name, x.storage)
        return s.lstrip(',')

    def get_service(self):
        servise_list = self.service.get_queryset()
        s = ''
        for x in servise_list:
            s +=',  %s' %(x.name)
        return s.lstrip(',')


    get_item.short_description = 'Проданый товар'
    get_service.short_description = 'Услуги/ремонт'

    def __str__(self):
        return self.sellerName

class Client(models.Model):
    """
    Таблица клиентов
    """
    name = models.CharField(max_length=255, verbose_name="Ф.И.О клиента")
    number_phone = models.CharField(max_length=13, verbose_name="Номер телефона",)
    email = models. EmailField(max_length=254, verbose_name="Електроный адрес ")
    discont = models.IntegerField(default=0)
    coment = models.CharField(max_length=255, verbose_name="Коментарий", blank=True)

    class Meta:
        verbose_name = "Клиент"
        verbose_name_plural = "Таблица клиентов"

